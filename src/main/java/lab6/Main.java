package lab6;

public class Main {
    public static void main(String[] args) {

        Randomize randomize = new Randomize();
        FileHandler fileHandler = new FileHandler();


        int N = 100;

        for (int j = 1; j<=5; j++) {
                fileHandler.writeToFile("Dla N = " + N*j);

            for (int i = 0; i < 20; i++) {

                // tablica N liczb dodatnich
                int[] array = randomize.getArrayWithRandomNumbersInRange(N*j, 100);

                // stworzenie binarnego drzewa wyszukiwań
                BinarySearchTree binarySearchTree = new BinarySearchTree();
                for (int k = 0; k < array.length; k++) {
                    binarySearchTree.insert(array[k]);
                }

                // dodanie losowego elementu do drzewa
                binarySearchTree.insert(randomize.getRandomNumberInRange(100));
                fileHandler.writeToFile("dodanie elementu: " + binarySearchTree.getInsertCounter());

                // wybranie losowego elementu z listy i usuniecie go z drzewa
                int elementToDelete = array[randomize.getRandomNumberInRange(array.length - 1)];
                binarySearchTree.delete(elementToDelete);
                fileHandler.writeToFile("usuniecie elementu: " + binarySearchTree.getDeleteCounter());

                // wybranie losowego elementu z listy i odnalezienie go w drzewie
                int elementToFind = array[randomize.getRandomNumberInRange(array.length - 1)];
                binarySearchTree.find(elementToFind);
                fileHandler.writeToFile("znalezienie elementu: " + binarySearchTree.getFindCounter());

            }

        }
    }
}
