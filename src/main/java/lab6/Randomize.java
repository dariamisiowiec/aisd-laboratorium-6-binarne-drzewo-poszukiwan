package lab6;

import java.util.Random;

public class Randomize {

    public int[] getArrayWithRandomNumbersInRange(int howManyNumbers, int maximumNumber) {
        int[] numbers = new int[howManyNumbers];
        Random random = new Random();
        for (int i = 0; i < howManyNumbers; i++) {
            numbers[i] = random.nextInt(maximumNumber+1);
        }
        return numbers;
    }

    public int getRandomNumberInRange(int maximumNumber) {
        Random random = new Random();
        return random.nextInt(maximumNumber + 1);
    }
}
