package lab6;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class FileHandler {

    private File fileName = new File("output.txt");

    public void writeToFile (String text) {

        try {
            PrintWriter writer = new PrintWriter(new FileWriter(fileName, true));
            writer.println(text);
            writer.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

}
